from django.conf.urls import url
from home import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^get_content/', views.get_content, name='get_content'),
]
