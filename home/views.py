import os
from django.shortcuts import render
from django.http import JsonResponse
from tasks.models import Task

# Create your views here.

def home(request):
    paths = os.path.expanduser('~').split('/')
    user = paths[2]
    return render(request, 'home/home.html', {'user': user})


def get_content(request):
    data = dict()
    data['files'] = list()
    paths = list()
    task = Task.objects.filter(is_selected=True,
                               status=Task.NEW_STATUS).first()
    if task:
        data['task_id'] = task.id
        paths = task.file_paths()
    if request.method == "GET":
        path = request.GET["path"]
        abspath = os.path.expanduser(path)
        try:
            files = os.listdir(abspath)
            is_in_task = False
            for file in files:
                full_path = os.path.join(abspath, file)
                type = 'dir' if os.path.isdir(full_path) else 'file'
                is_in_task = full_path in paths
                data['files'].append({'type': type,
                                      'name': file,
                                      'fullpath': full_path,
                                      'is_in_task': is_in_task})
            data['status'] = True
        except Exception:
            data['status'] = False
    return JsonResponse(data)
