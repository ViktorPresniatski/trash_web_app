import uwsgi
from uwsgidecorators import mulefunc
import pathos.multiprocessing as mp
from utilite_trash.actions.actions import Action
import os
import logging


@mulefunc
def execute_task(task_id):
    logging.warning("Mule id: {0}; Process id: {1}".format(uwsgi.mule_id(), os.getpid()))
    from tasks.models import Task
    from tasks.models import FileItemToRemove
    def delete(path):
        logging.warning("Pool: Process id: {}".format(os.getpid()))
        status = action.remove([path], one=True)
        return {'path': path, 'status': status}

    def callback(obj):
        if obj['status']:
            FileItemToRemove.objects.filter(path=obj['path']).first().removed()

    task = Task.objects.get(id=task_id)
    action = Action(
        trash_path=task.trash.path,
        trash_max_size=task.trash.max_size,
        trash_name_duplicate=task.trash.name_duplicate,
        clear_by_date=task.trash.clear_by_date_policy,
        clear_by_size=task.trash.clear_by_size_policy,
        remove_policy=task.policy_name
    )

    task.run()
    processes = mp.cpu_count() * 2
    pool = mp.Pool(processes=processes)
    paths = [f.path for f in task.files.all()]
    for path in paths:
        pool.apply_async(delete, args=(path, ), callback=callback)
    pool.close()
    pool.join()
    task.refresh_from_db()
    task.finish()
