function getDirContent(path) {
    // var func = function(){
    $.get('/get_content', {'path': path}, function(data){
        console.log(data);
        if (!data.status)
            return false;
        console.log("OK: files received");
        data.files.sort(function(a, b){
            if (a.type == "dir" && b.type == "file") return -1;
            if (a.type == "file" && b.type == "dir") return 1;
            if (a.type == b.type) {
                if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;
                if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;
            }
        });
        $('#items-place-holder').empty();
        csrf_token = $.cookie("csrftoken");
        task_id = data.task_id;
        console.log('task id: ' + task_id);
        var method = ''
        var btn_type = ''
        if (task_id) {
            method = 'post';
            btn_type = 'submit';
        }
        else {
            btn_type = 'reset';
        }
        data.files.forEach(function(item) {
            if (item.is_in_task) {
                url = `tasks/remove_from_task/${task_id}`;
                button_class = 'warning';
                icon_class = 'minus';
            }
            else {
                url = `tasks/add_to_task/${task_id}`;
                button_class = 'success';
                icon_class = 'plus';
            }
            icon_type = item.type == 'dir' ? 'folder-close' : 'file';
            if (task_id) {
                form_string = `<form method="${method}" class="toggle-file-task-form" action="${url}">
                                   <input type="hidden" name="csrfmiddlewaretoken"
                                        value="${csrf_token}">
                                   <input type="hidden" name="fullpath" value="${item.fullpath}">
                                   <button type="${btn_type}" class="btn-${button_class}">
                                       <span class="glyphicon glyphicon-${icon_class}"></span>
                                   </button>
                               </form>`
            }
            else {
                form_string = `<button class="btn btn-sm btn-default">
                                   <span class="glyphicon glyphicon-remove"></span>
                               </button>`
            }

            html = `<div class="file-element ${item.type}">
                        <div>
                            <span class="glyphicon glyphicon-${icon_type}" aria-hidden="true">
                            </span>
                            ${item.name}
                        </div>
                        ${form_string}
                    </div>`;
            $('#items-place-holder').append(html);
        })
        if (!$('#hidden').is(':checked')) {
            $('#items-place-holder').children().each(function(){
                if ($.trim($(this).text()).startsWith('\.')) {
                    $(this).attr('style', 'display: none');
                }
            });
        }
    })//.then(function(){
        //     console.log('1')
        // }, function(){
        //     console.log('2')
        // })
        // return true;
    // }
    // return doFunc ? func() : func;
}

var loadForm = function(e) {
    e.stopPropagation();
    var btn = $(this);
    $.ajax({
        url: btn.attr("data-url"),
        type: 'get',
        dataType: 'json',
            beforeSend: function () {
            $("#modal-item").modal("show");
        },
        success: function (data) {
            $("#modal-item .modal-content").html(data.html_form);
        }
    });
};
$('.table-borderd').click(function(){
    var id = $(this).find('.transaction').text()
    id = $.trim(id)
    $(`#modal-${id}`).modal.style.display.block
})
var saveForm = function(e) {
    e.preventDefault()
    var form = $(this);
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: form.attr("method"),
        dataType: 'json',
        cache: true,
        success: function (data) {
            if (data.form_is_valid) {
                $("#item-list").html(data.html_list);
                $("#modal-item").modal("hide");
            }
            else {
                $("#modal-item .modal-content").html(data.html_form);
            }
        }
    });
};

var toggleFileInTask = function(e) {
    e.stopPropagation();
    e.preventDefault();

    form = $(this);
    $.ajax({
        url: form.attr('action'),
        data: form.serialize(),
        type: form.attr('method'),
        dataType: 'json',
        cache: true,
        success: function(data) {
            if (data.status) {
                data_form = form.serializeArray()

                var url = form.attr('action');
                var csrf_token = data_form[0]['value'];
                var fullpath = data_form[1]['value'];
                var button_class = '';
                var icon_class = '';
                if (url.includes('add_to')){
                    url = url.replace('add_to', 'remove_from');
                    button_class = 'warning';
                    icon_class = 'minus';
                    console.log("added " + data_form[1]['value']);
                }
                else {
                    url = url.replace('remove_from', 'add_to');
                    button_class = 'success';
                    icon_class = 'plus';
                    console.log("removed " + data_form[1]['value']);
                }
                form.html(`<input type="hidden" name="csrfmiddlewaretoken"
                               value="${csrf_token}">
                           <input type="hidden" name="fullpath" value="${fullpath}">
                           <button type="submit" class="btn-${button_class}">
                               <span class="glyphicon glyphicon-${icon_class}"></span>
                           </button>`);
                form.attr('action', url);
            }
        }
    });
}
