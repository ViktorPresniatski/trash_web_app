$(document).ready(function(){
    $('#items-place-holder').on('click', '.dir', function(e){
        var fullpath = '/';
        var dest = $.trim($(this).text());
        $('ol.breadcrumb').children('li').each(function(i){
            if (i != 0) {
                fullpath += $(this).text() + '/';
            }
            if ($(this)[0].classList.contains('active')) return false;
        });
        getDirContent(fullpath + dest)
            // console.log("TOP")
        active = $('.breadcrumb .active');
        while (active[0].nextElementSibling) {
            active[0].nextElementSibling.remove();
        }
        active.removeClass('active').addClass('relative-path');
        $('.breadcrumb').append(`<li class="active">${dest}</li>`);
        // if (!status)
        //     return false;

    });

    $(document).on('click', '.relative-path', function(e){
        e.preventDefault();
        var active = $('.breadcrumb .active');
        active.addClass('relative-path').removeClass('active');
        $(this).addClass('active').removeClass('relative-path');
        $(this).text($(this).text());
        var newpath = '/';
        $('ol.breadcrumb').children('li').each(function(i){
            if (i != 0) {
                newpath += $(this).text() + '/';
            }
            if ($(this)[0].classList.contains('active')) return false;
        });
        getDirContent(newpath);
    });

    $(document).on('click', '#hidden', function(e){
        $('#items-place-holder').children().each(function(){
            if ($.trim($(this).text()).startsWith('\.')) {
                $(this).toggle();
            }
        });
    });

    $('#item-list').on('click', '.item', function(){
        window.location = $(this).attr("href");
    });

    $('#items-place-holder').on('click', '.toggle-file-task-form', toggleFileInTask);
    $("#item-list").on('click', '.js-delete-item', loadForm);
    $("#modal-item").on("submit", ".js-item-delete-form", saveForm);

    console.log("debug");
});
