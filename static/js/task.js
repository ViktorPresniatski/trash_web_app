$(document).ready(function(){
    $('#task-select').on('submit', function(e) {
        e.preventDefault();
        form = $(this)
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            dataType: 'json',
            cache: true,
            success: function(data) {
                if (data.status) {
                    form.html('<button type="button" class="btn btn-success">Select</button>')
                }
            }
        });
    });

    $('.remove-from-file-list-btn').on('submit', function(e) {
        e.preventDefault();
        form = $(this);
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            dataType: 'json',
            cache: true,
            success: function(data) {
                if (data.status) {
                    form.parent().parent().remove();
                }
            }
        });
    });
});
