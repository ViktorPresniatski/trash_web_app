$(document).ready(function(){
    $('.trashed-file-action').on('submit', function(e){
        e.preventDefault();
        form = $(this)
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            dataType: 'json',
            cache: true,
            success: function(data) {
                if (data.status) {
                    console.log(data.status);
                    form.parent().parent().parent().remove()
                }
            }
        });
    });
});
