from django.contrib import admin
from tasks.models import Task, FileItemToRemove

# Register your models here.

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'trash', 'status', 'is_selected')


@admin.register(FileItemToRemove)
class FileAdmin(admin.ModelAdmin):
    list_display = ('path', 'task', 'is_removed')

