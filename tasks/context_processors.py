from tasks.models import Task


def get_selected_task(request):
    selected_task = Task.objects.filter(is_selected=True).first()
    return {'selected_task': selected_task}
