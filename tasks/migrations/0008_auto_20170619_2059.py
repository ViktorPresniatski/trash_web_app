# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-06-19 20:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0007_auto_20170615_2258'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='force_policy',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='task',
            name='interactive_policy',
            field=models.BooleanField(default=False),
        ),
    ]
