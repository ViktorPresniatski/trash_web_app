from django.db import models
from trash.models import Trash

# Create your models here.

class Task(models.Model):
    NEW_STATUS = 0
    PENDING_STATUS = 1
    PROCESSING_STATUS = 2
    COMPLETE_STATUS = 3
    FAILURE_STATUS = 4
    STATUS_CHOICES = (
        (NEW_STATUS, 'New'),
        (PENDING_STATUS, 'Pending'),
        (PROCESSING_STATUS, 'Processing'),
        (COMPLETE_STATUS, 'Complete'),
        (FAILURE_STATUS, 'Failure'),
    )

    name = models.CharField(max_length=255, blank=True, null=True)
    trash = models.ForeignKey(
        Trash,
        on_delete=models.CASCADE,
        related_name="tasks",
    )
    status = models.IntegerField(choices=STATUS_CHOICES, default=NEW_STATUS)
    is_selected = models.BooleanField(default=False)

    DEFAULT_POLICY = 0
    FORCE_POLICY = 1
    POLICY_CHOICES = (
        (DEFAULT_POLICY, "default"),
        (FORCE_POLICY, "force"),
    )
    policy = models.IntegerField(choices=POLICY_CHOICES,default=DEFAULT_POLICY)

    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def select(self):
        if not self.status == self.NEW_STATUS:
            return
        active = Task.objects.filter(is_selected=True)
        if active:
            active.update(is_selected=False)
        self.is_selected = True
        self.save()

    def reset(self):
        self.files.all().delete()
        self.status = self.NEW_STATUS
        self.is_selected = False
        self.save()

    @property
    def status_name(self):
        return self.STATUS_CHOICES[self.status][1]

    @property
    def policy_name(self):
        return self.POLICY_CHOICES[self.policy][1]

    def enqueue(self):
        self.is_selected = False
        self.status = self.PENDING_STATUS
        self.save()

    def run(self):
        self.status = self.PROCESSING_STATUS
        self.save()

    def finish(self):
        self.status = self.COMPLETE_STATUS
        self.save()

    def failure(self):
        self.status = self.FAILURE_STATUS
        self.save()

    def file_paths(self):
        return [el.path for el in self.files.all()]

    def __str__(self):
        return "Task: id: {0}, name: {1}".format(self.id, self.name)


class FileItemToRemove(models.Model):
    path = models.CharField(max_length=255)
    task = models.ForeignKey(Task,
        on_delete=models.CASCADE,
        related_name="files",
    )
    is_removed = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def removed(self):
        self.is_removed = True
        self.save()

    def __str__(self):
        return "File: id:{0}, path: {1}".format(self.id, self.path)

    class Meta:
        unique_together = ('path', 'task')
