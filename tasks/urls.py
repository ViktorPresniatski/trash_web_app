from django.conf.urls import url
from tasks import views


urlpatterns = [
    url(r'^$', views.task_index, name='task_index'),
    url(r'^new$', views.task_create, name='task_create'),
    url(r'^edit/(?P<pk>\d+)$', views.task_update, name='task_update'),
    url(r'^delete/(?P<pk>\d+)$', views.task_delete, name='task_delete'),
    url(r'^show/(?P<pk>\d+)$', views.task_show, name='task_show'),
    url(r'^add_to_task/(?P<task_id>\d+)$', views.add_to_task, name='add_to_task'),
    url(r'^remove_from_task/(?P<task_id>\d+)$', views.remove_from_task, name='remove_from_task'),
    url(r'^task_select/(?P<pk>\d+)$', views.task_select, name='task_select'),
    url(r'^execute/(?P<pk>\d+)$', views.task_execute, name='task_execute'),
    url(r'^reset/(?P<pk>\d+)$', views.task_reset, name='task_reset'),
    url(r'^add_by_regex/(?P<pk>\d+)$', views.add_by_regex, name='add_by_regex'),
]
