from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse
from tasks.models import Task, FileItemToRemove
from tasks.forms import TaskForm
from utilite_trash.models.trash import Trash
from utilite_trash.actions.actions import Action
from mytasks.task import execute_task

# Create your views here.

def task_index(request):
    tasks = Task.objects.all()
    return render(request, 'tasks/index.html', locals())


def task_create(request):
    form = TaskForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        task = form.save()
        return redirect('task_show', pk=task.pk)
    return render(request, 'tasks/new.html', locals())


def task_update(request, pk):
    task = get_object_or_404(Task, pk=pk)
    form = TaskForm(request.POST or None, instance=task)
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('task_show', pk=task.pk)
    return render(request, 'tasks/update.html', locals())


def task_delete(request, pk):
    task = get_object_or_404(Task, pk=pk)
    data = dict()
    if request.method == 'POST':
        task.delete()
        data['form_is_valid'] = True
        tasks = Task.objects.all()
        data['html_list'] = render_to_string('tasks/_list.html',
            {'tasks': tasks}
        )
    else:
        context = {'task': task}
        data['html_form'] = render_to_string('tasks/delete.html',
            context,
            request=request,
        )
    return JsonResponse(data)


def task_show(request, pk):
    task = get_object_or_404(Task, pk=pk)
    task_files = task.files.all()
    return render(request, 'tasks/show.html', locals())


def task_select(request, pk):
    data = dict()
    task = get_object_or_404(Task, pk=pk)
    if request.method == 'POST':
        task.select()
        if task.is_selected:
            data['status'] = True
        else:
            data['status'] = False
    return JsonResponse(data)


def add_to_task(request, task_id):
    data = dict()
    task = get_object_or_404(Task, pk=task_id)
    if request.method == 'POST' and task.is_selected:
        fullpath = request.POST['fullpath']
        try:
            FileItemToRemove.objects.create(path=fullpath, task=task)
            data['status'] = True
        except Exception:
            data['status'] = False
    return JsonResponse(data)


def remove_from_task(request, task_id):
    data = dict()
    task = get_object_or_404(Task, pk=task_id)
    if request.method == 'POST' and task.is_selected:
        fullpath = request.POST['fullpath']
        file = task.files.filter(path=fullpath).first()
        if file:
            file.delete()
            data['status'] = True
        else:
            data['status'] = False
    return JsonResponse(data)


def task_reset(request, pk):
    task = get_object_or_404(Task, pk=pk)
    if request.method == 'POST':
        if not task.status in [Task.PENDING_STATUS, Task.PROCESSING_STATUS]:
            task.reset()
    return redirect('task_show', pk=task.pk)


def task_execute(request, pk):
    task = get_object_or_404(Task, pk=pk)
    if request.method == 'POST' and \
       task.status == Task.NEW_STATUS and \
       task.files.filter(is_removed=False):
        task.enqueue()
        execute_task(task_id=task.id)
    return redirect('task_index')


def add_by_regex(request, pk):
    data = dict()
    task = get_object_or_404(Task, pk=pk)
    if request.method == 'POST' and task.status == Task.NEW_STATUS:
        trash = Trash(path=task.trash.path)
        data = request.POST
        files = trash.grep(data['directory'], data['regex'])
        for file in files:
            try:
                FileItemToRemove.objects.create(path=file, task=task)
            except Exception:
                pass
    return redirect('task_show', pk=task.pk)
