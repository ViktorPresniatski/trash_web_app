from django.contrib import admin
from trash.models import Trash

# Register your models here.

@admin.register(Trash)
class TrashAdmin(admin.ModelAdmin):
    list_display = ('path', 'max_size')
