from django import forms
from trash.models import Trash

class TrashForm(forms.ModelForm):
    class Meta:
        model = Trash
        exclude = [""]
