# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-06-19 18:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trash', '0003_auto_20170619_1709'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trash',
            name='clear_by_date_policy',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
