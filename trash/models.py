from __future__ import unicode_literals

from django.core.validators import MinValueValidator
from django.db import models

# Create your models here.

class Trash(models.Model):
    GET_UNIQ = 0
    REPLACE = 1
    DUPLICATE_CHOICES = (
        (GET_UNIQ, "get_uniq"),
        (REPLACE, "replace"),
    )

    path = models.CharField(max_length=255, unique=True)
    max_size = models.IntegerField(blank=True, null=True,
        validators=[MinValueValidator(1)])
    name_duplicate_policy = models.IntegerField(choices=DUPLICATE_CHOICES,
                                                default=GET_UNIQ)
    clear_by_date_policy = models.IntegerField(blank=True, null=True,
        validators=[MinValueValidator(1)])
    clear_by_size_policy = models.IntegerField(blank=True, null=True,
        validators=[MinValueValidator(1)])
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    @property
    def name_duplicate(self):
        return self.DUPLICATE_CHOICES[self.name_duplicate_policy][1]

    def __str__(self):
        return "Trash: path: {}".format(self.path)
