from django.conf.urls import url
from trash import views

urlpatterns = [
    url(r'^$', views.trash_index, name='trash_index'),
    url(r'^new$', views.trash_create, name='trash_create'),
    url(r'^edit/(?P<pk>\d+)$', views.trash_update, name='trash_update'),
    url(r'^delete/(?P<pk>\d+)$', views.trash_delete, name='trash_delete'),
    url(r'^show/(?P<pk>\d+)$', views.trash_show, name='trash_show'),
    url(r'^show/(?P<pk>\d+)/trashed_file_clear$', views.trashed_file_clear,
        name="trashed_file_clear"),
    url(r'^show/(?P<pk>\d+)/trashed_file_restore$', views.trashed_file_restore,
        name="trashed_file_restore"),
    url(r'^restore_all/(?P<pk>\d+)$', views.restore_all, name="restore_all"),
    url(r'^clear_all/(?P<pk>\d+)$', views.clear_all, name="clear_all"),
]
