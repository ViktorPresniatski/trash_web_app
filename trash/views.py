from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from trash.models import Trash
from trash.forms import TrashForm
from django.http import JsonResponse
from utilite_trash.models.trash import Trash as TrashModel
from utilite_trash.actions.actions import Action

# Create your views here.

def trash_index(request):
    list_of_trash = Trash.objects.all()
    return render(request, 'trash/index.html', locals())


def trash_create(request):
    form = TrashForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        trash = form.save(commit=False)
        trash.path = '~/{}'.format(trash.path)
        trash.save()
        return redirect('trash_index')
    return render(request, 'trash/new.html', locals())


def trash_update(request, pk):
    trash = get_object_or_404(Trash, pk=pk)
    form = TrashForm(request.POST or None, instance=trash)
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('trash_index')
    return render(request, 'trash/update.html', locals())


def trash_delete(request, pk):
    trash = get_object_or_404(Trash, pk=pk)
    data = dict()
    if request.method == 'POST':
        trash.delete()
        data['form_is_valid'] = True
        list_of_trash = Trash.objects.all()
        data['html_list'] = render_to_string('trash/_list.html',
            {'list_of_trash': list_of_trash}
        )
    else:
        context = {'trash': trash}
        data['html_form'] = render_to_string('trash/delete.html',
            context,
            request=request,
        )
    return JsonResponse(data)


def trash_show(request, pk):
    trash = get_object_or_404(Trash, pk=pk)
    action = Action(
        trash_path=trash.path,
        clear_by_date=trash.clear_by_date_policy,
        clear_by_size=trash.clear_by_size_policy
    )
    page = action.trash.show()
    return render(request, 'trash/show.html', locals())


def trashed_file_clear(request, pk):
    data = dict()
    if request.method == 'POST':
        name = request.POST['filename']
        trash_path = get_object_or_404(Trash, pk=pk).path
        trash = TrashModel(path=trash_path)
        data['status'] = trash.clear(name)
    return JsonResponse(data)


def trashed_file_restore(request, pk):
    data = dict()
    if request.method == 'POST':
        name = request.POST['filename']
        trash_path = get_object_or_404(Trash, pk=pk).path
        trash = TrashModel(path=trash_path)
        data['status'] = trash.restore(name)
    return JsonResponse(data)

def restore_all(request, pk):
    trash = get_object_or_404(Trash, pk=pk)
    if request.method == 'POST':
        action = Action(trash_path=trash.path,
            clear_by_date=trash.clear_by_date_policy,
            clear_by_size=trash.clear_by_size_policy
        )
        action.run(operation="restore", all=True)
    return redirect("trash_show", pk=pk)

def clear_all(request, pk):
    trash = get_object_or_404(Trash, pk=pk)
    if request.method == 'POST':
        action = Action(trash_path=trash.path,
            clear_by_date=trash.clear_by_date_policy,
            clear_by_size=trash.clear_by_size_policy
        )
        action.run(operation="clear", all=True)
    return redirect("trash_show", pk=pk)
